resource "boundary_auth_method_oidc" "provider" {
  name                 = var.name
  description          = "OIDC auth method for Auth0"
  scope_id             = var.scope_id
  issuer               = var.issuer
  client_id            = var.client_id
  client_secret        = var.client_secret
  signing_algorithms   = ["RS256"]
  api_url_prefix       = "http://localhost:9200"
  is_primary_for_scope = true
  state                = "active-public"
  max_age              = 0
}

resource "boundary_account_oidc" "oidc_user" {
  name           = "user1"
  description    = "OIDC account for user1"
  auth_method_id = boundary_auth_method_oidc.provider.id
  issuer         = var.issuer
  subject        = var.subject
}
