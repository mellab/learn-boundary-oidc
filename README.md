Repositorio utilizada para la presentación ["Confianza Cero: No confíes ni en tu propia sombra"](https://docs.google.com/presentation/d/1LsmCBkoL7GFSJCq8snLW29OrEUzbOcZYSzxj5h1CG3o/edit?usp=sharing), HashiTalks: Latino América 2022.

Este repositorio está basado en el laboratorio de [Autenticación OIDC](https://learn.hashicorp.com/tutorials/boundary/oidc-auth?in=boundary/configuration) disponible en [HashiCorp Learn](https://learn.hashicorp.com/).
