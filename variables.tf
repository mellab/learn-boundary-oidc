variable "name" {
  type = string
}

variable "scope_id" {
  type = string
}

variable "issuer" {
  type = string
}

variable "client_id" {
  type      = string
  sensitive = true
}

variable "client_secret" {
  type      = string
  sensitive = true
}

variable "subject" {
  type      = string
  sensitive = true
}
