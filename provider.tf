terraform {
  required_providers {
    boundary = {
      source  = "hashicorp/boundary"
      version = "1.0.5"
    }
  }
}

provider "boundary" {
  addr             = "http://127.0.0.1:9200"
  recovery_kms_hcl = <<EOT
kms "aead" {
  purpose = "recovery"
  aead_type = "aes-gcm"
  key = "7oOqNc9vehN4E36hPy9EDQHoI+GuLFdxSU+4qYWKU4s="
  key_id = "global_recovery"
}
EOT
}
