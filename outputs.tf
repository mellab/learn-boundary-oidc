output "auth-method-id" {
  value     = boundary_auth_method_oidc.provider.id
  sensitive = true
}
